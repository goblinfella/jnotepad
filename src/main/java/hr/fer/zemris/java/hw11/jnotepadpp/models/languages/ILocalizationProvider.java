package hr.fer.zemris.java.hw11.jnotepadpp.models.languages;

/**
 * Class that models a provider of localized strings.
 * @author Fabijan Džapo
 *
 */
public interface ILocalizationProvider {
	
	/**
	 * Get localized string
	 * @param key
	 * @return string
	 */
	String getString(String key);
	
	/**
	 * Add listener
	 * @param listener
	 */
	void addLocalizationListener(ILocalizationListener l);
	
	/**
	 * Remove listener
	 * @param listener
	 */
	void removeLocalizationListener(ILocalizationListener l);
	
	/**
	 * Retrieve current language
	 * @return language
	 */
	String getCurrentLanguage();

}
