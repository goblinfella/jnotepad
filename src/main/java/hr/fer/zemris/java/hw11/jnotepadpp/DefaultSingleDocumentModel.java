package hr.fer.zemris.java.hw11.jnotepadpp;

import java.nio.file.Path;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;

/**
 * Class that models the default implementation of a single document model.
 * @author Fabijan Džapo
 *
 */
public class DefaultSingleDocumentModel implements SingleDocumentModel {
	
	/**
	 * Path to file where the doc can be saved
	 */
	private Path path;
	
	/**
	 * Has the document been modified?
	 */
	private boolean isModified;
	
	/**
	 * Text area that displays the document contents
	 */
	private JTextArea textArea;
	
	/**
	 * Listeners of changes
	 */
	private List<SingleDocumentListener> listeners;

	/**
	 * Constructor, takes a path to file and text contents
	 * @param path
	 * @param text
	 */
	public DefaultSingleDocumentModel(Path path, String text) {
		super();
		this.path = path;
		this.isModified = false;
		this.textArea = new JTextArea(text);
		textArea.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				setModified(true);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				setModified(true);
			}
			@Override
			public void changedUpdate(DocumentEvent e) {
				setModified(true);
			}
		});
		this.listeners = new LinkedList<>();
	}

	@Override
	public JTextArea getTextComponent() {
		return textArea;
	}

	@Override
	public Path getFilePath() {
		return path;
	}

	@Override
	public void setFilePath(Path path) {
		if (path == null) {
			throw new IllegalArgumentException("Path cannot be null.");
		}
		this.path = path;
		notifyListenersForPath();
	}

	@Override
	public boolean isModified() {
		return isModified;
	}

	@Override
	public void setModified(boolean modified) {
		isModified = modified;
		notifyListenersForModification();
	}

	@Override
	public void addSingleDocumentListener(SingleDocumentListener l) {
		if (l == null) {
			throw new IllegalArgumentException("Listener cannot be null.");
		}
		listeners = new LinkedList<>(listeners);
		listeners.add(l);
	}

	@Override
	public void removeSingleDocumentListener(SingleDocumentListener l) {
		if (l == null) {
			throw new IllegalArgumentException("Listener cannot be null.");
		}
		listeners = new LinkedList<>(listeners);
		listeners.remove(l);
	}
	
	private void notifyListenersForModification() {
		for (SingleDocumentListener listener : listeners) {
			listener.documentModifyStatusUpdated(this);
		}
	}
	
	private void notifyListenersForPath() {
		for (SingleDocumentListener listener : listeners) {
			listener.documentFilePathUpdated(this);;
		}
	}
}
