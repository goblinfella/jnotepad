package hr.fer.zemris.java.hw11.jnotepadpp.models;

/**
 * Class modeling an observer of {@link SingleDocumentModel}
 * @author Fabijan Džapo
 *
 */
public interface SingleDocumentListener {
	
	/**
	 * The document has been modified.
	 * @param model
	 */
	void documentModifyStatusUpdated(SingleDocumentModel model);
	
	/**
	 * The path to where the document has been saved has been changed.
	 * @param model
	 */
	void documentFilePathUpdated(SingleDocumentModel model);
}
