package hr.fer.zemris.java.hw11.jnotepadpp.models;

/**
 * Interface modeling an observer of {@link MultipleDocumentModel}
 * @author Fabijan Džapo
 *
 */
public interface MultipleDocumentListener {
	
	/**
	 * Focus was shifted from one document to another.
	 * @param previousModel
	 * @param currentModel
	 */
	void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel);
	
	/**
	 * A document was added
	 * @param model
	 */
	void documentAdded(SingleDocumentModel model);
	
	/**
	 * A document was closed
	 * @param model
	 */
	void documentRemoved(SingleDocumentModel model);
}
