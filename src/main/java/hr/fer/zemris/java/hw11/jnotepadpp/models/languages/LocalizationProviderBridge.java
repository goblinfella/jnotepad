package hr.fer.zemris.java.hw11.jnotepadpp.models.languages;

public class LocalizationProviderBridge extends AbstractLocalizationProvider {
	
	private ILocalizationProvider parent;
	
	private boolean connected;
	
	private ILocalizationListener listener;

	public LocalizationProviderBridge(ILocalizationProvider parent) {
		super();
		this.connected = false;
		this.parent = parent;
	}

	@Override
	public String getString(String key) {
		return parent.getString(key);
	}
	
	public void connect() {
		if (!connected) {
			listener = new ILocalizationListener() {
				@Override
				public void localizationChanged() {
					fire();
				}
			};
			parent.addLocalizationListener(listener);
			connected = true;
		}
	}
	
	public void disconnect() {
		if (connected) {
			parent.removeLocalizationListener(listener);
			connected = false;
		}
	}

	@Override
	public String getCurrentLanguage() {
		return parent.getCurrentLanguage();
	}

}
