package hr.fer.zemris.java.hw11.jnotepadpp.models.languages;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

/**
 * Class modeling an abstract provider who can only manage listeners
 * @author Fabijan Džapo
 *
 */
public abstract class AbstractLocalizationProvider implements ILocalizationProvider {
	
	/**
	 * Listeners
	 */
	private List<ILocalizationListener> listeners;
	
	/**
	 * Constructor, must be called by subclasses
	 */
	public AbstractLocalizationProvider() {
		listeners = new LinkedList<ILocalizationListener>();
	}

	@Override
	public void addLocalizationListener(ILocalizationListener l) {
		Objects.requireNonNull(l, "Listener cannot be null.");
		listeners = new LinkedList<>(listeners);
		listeners.add(l);
	}

	@Override
	public void removeLocalizationListener(ILocalizationListener l) {
		Objects.requireNonNull(l, "Listener cannot be null.");
		listeners = new LinkedList<>(listeners);
		listeners.remove(l);
	}
	
	/**
	 * Notify all listeners
	 */
	public void fire() {
		for (ILocalizationListener l : listeners) {
			l.localizationChanged();
		}
	}

}
