package hr.fer.zemris.java.hw11.jnotepadpp.models.languages;

import javax.swing.JMenu;

/**
 * JMenu that can update it's text based on localized settings.
 * @author Fabijan Džapo
 *
 */
@SuppressWarnings("serial")
public class LJMenu extends JMenu {

	public LJMenu(String key, ILocalizationProvider lp) {
		setText(lp.getString(key));
		lp.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				setText(lp.getString(key));
			}
		});
	}
	
}
