package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JLabel;
import javax.swing.JToolBar;
import javax.swing.SwingUtilities;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.Caret;

import hr.fer.zemris.java.hw11.jnotepadpp.models.languages.LocalizationProvider;

/**
 * Class that models a custom toolbar that displays information
 * @author Fabijan Džapo
 *
 */
@SuppressWarnings("serial")
public class InformartionToolbar extends JToolBar implements ChangeListener {
	
	/**
	 * Size of text in document
	 */
	private int length;
	
	/**
	 * Line the caret is positioned in
	 */
	private int line;
	
	/**
	 * Column the caret is positioned in
	 */
	private int col;
	
	/**
	 * Size of selected text
	 */
	private int sel;
	
	/**
	 * Editor
	 */
	private JNotepadPP notepad;
	
	/**
	 * Label displaying size
	 */
	private JLabel lenLabel;
	
	/**
	 * Label displaying statistics
	 */
	private JLabel statLabel;
	
	/**
	 * Label displaying time
	 */
	private JLabel clockLabel;
	
	/**
	 * Daemon clock thread
	 */
	private Thread clock;
	
	/**
	 * Boolean to stop the thread
	 */
	private AtomicBoolean go;
	
	private DateTimeFormatter formatter;
	
	private LocalizationProvider provider;

	/**
	 * Constructor, takes only the parent notepad
	 * @param notepad
	 */
	public InformartionToolbar(JNotepadPP notepad) {
		super();
		this.notepad = notepad;

		go = new AtomicBoolean(true);
		formatter = DateTimeFormatter.ofPattern("hh:mm:ss");
		provider = LocalizationProvider.getInstance();
		
		initGUI();
	}

	/**
	 * Crate labels
	 */
	private void initGUI() {
		setLayout(new BorderLayout());
		lenLabel = new JLabel();
		add(lenLabel, BorderLayout.LINE_START);
		statLabel = new JLabel("", SwingUtilities.CENTER);
		add(statLabel, BorderLayout.CENTER);
		clockLabel = new JLabel();
		add(clockLabel, BorderLayout.LINE_END);
		
		clock = new Thread(()->{
			while(go.getAcquire()) {
				try {
					Thread.sleep(500);
				} catch(Exception ex) {}
				SwingUtilities.invokeLater(()->{
					updateTime();
				});
			}
		});
		clock.setDaemon(true);
		clock.start();
		
		updateLabel();
	}
	
	/**
	 * Update time on display
	 */
	private void updateTime() {
		clockLabel.setText(formatter.format(LocalTime.now()));
	}

	/**
	 * Update statistics on labels
	 */
	private void updateLabel() {
		lenLabel.setText(provider.getString("InBarLen") + length);
		statLabel.setText(String.format(provider.getString("InBarInfo"), line + 1, col + 1, sel));
	}



	/**
	 * Class used to gather info on the Caret position.
	 * @author Fabijan Džapo
	 *
	 */
	public static class StatInfo {
		int line;
		int colm;
		int length;
		/**
		 * constructor
		 * @param line
		 * @param colm
		 * @param length
		 */
		public StatInfo(int line, int colm, int length) {
			super();
			this.line = line;
			this.colm = colm;
			this.length = length;
		}
	}



	@Override
	public void stateChanged(ChangeEvent e) {
		Caret car = (Caret) e.getSource();
		int mark = car.getMark();
		int dot = car.getDot();
		sel = Math.abs(mark - dot);
		
		StatInfo si = notepad.getStatInfo(dot);
		
		length = si.length;
		line = si.line;
		col = si.colm;
		
		updateLabel();
	}
	
	/**
	 * Kill daemon thread
	 */
	protected void killClock() {
		go.set(false);
	}

}
