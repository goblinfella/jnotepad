package hr.fer.zemris.java.hw11.jnotepadpp.models;

import java.nio.file.Path;

import javax.swing.JTextArea;

/**
 * Class modeling a Single document in a word editor.
 * The model is a subject and can have observers
 * @author Fabijan Džapo
 *
 */
public interface SingleDocumentModel {
	
	/**
	 * Returns the text area
	 * @return text area
	 */
	JTextArea getTextComponent();
	
	/**
	 * Returns the path to where the text can be saved
	 * @return path
	 */
	Path getFilePath();
	
	/**
	 * Sets the path to file, cannot be null
	 * @param path
	 * 
	 * @throws NullPointerException
	 */
	void setFilePath(Path path);
	
	/**
	 * Checks whether a file has been modified or not
	 * @return true if modified
	 */
	boolean isModified();
	
	/**
	 * Sets the modification status of document
	 * @param modified
	 */
	void setModified(boolean modified);
	
	/**
	 * Add listener to observer
	 * @param l
	 */
	void addSingleDocumentListener(SingleDocumentListener l);
	
	/**
	 * Remove listener from observer
	 * @param l
	 */
	void removeSingleDocumentListener(SingleDocumentListener l);
	
}
