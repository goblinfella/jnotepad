package hr.fer.zemris.java.hw11.jnotepadpp.models;

import java.nio.file.Path;

/**
 * Class modeling multiple documents in a word editor.
 * This class is a subject.
 * @author Fabijan Džapo
 *
 */
public interface MultipleDocumentModel extends Iterable<SingleDocumentModel>{

	/**
	 * Create and open a blank document
	 * @return blank document
	 */
	SingleDocumentModel createNewDocument();
	
	/**
	 * Returns document currently being edited
	 * @return current document
	 */
	SingleDocumentModel getCurrentDocument();
	
	/**
	 * Open an existing document
	 * @return existing document
	 */
	SingleDocumentModel loadDocument(Path path);
	
	/**
	 * Save a document to a file. Prioritize {@code newPath} but newPath can be null,
	 * if null save to path of{@code model}.
	 * Both paths cannot be null.
	 * @param model to be saved
	 * @param newPath to save the document to
	 */
	void saveDocument(SingleDocumentModel model, Path newPath);
	
	/**
	 * Close current document
	 * @param model
	 */
	void closeDocument(SingleDocumentModel model);
	
	/**
	 * Add listener
	 * @param listener
	 */
	void addMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Remove listener
	 * @param listener
	 */
	void removeMultipleDocumentListener(MultipleDocumentListener l);
	
	/**
	 * Returns number of active documents
	 * @return number of documents
	 */
	int getNumberOfDocuments();
	
	/**
	 * Retrieve a document from index
	 * @param index
	 * @return document
	 */
	SingleDocumentModel getDocument(int index);
}
