package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.nio.file.Path;
import java.text.Collator;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.function.Function;

import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Caret;
import javax.swing.text.Document;

import hr.fer.zemris.java.hw11.jnotepadpp.InformartionToolbar.StatInfo;
import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.languages.FormLocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.languages.ILocalizationListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.languages.LJMenu;
import hr.fer.zemris.java.hw11.jnotepadpp.models.languages.LocalizableAction;
import hr.fer.zemris.java.hw11.jnotepadpp.models.languages.LocalizationProvider;

/**
 * This class is models a somewhat rudimentary word editor.
 * It can open existing file, create new ones and save files.
 * It also offers a few simple editing tools and can display
 * statistics and information.
 * @author Fabijan Džapo
 *
 */
@SuppressWarnings("serial")
public class JNotepadPP extends JFrame {
	
	/**
	 * Model of all documents in editor
	 */
	private DefaultMultipleDocumentModel docModel;
	
	/**
	 * Buffer used copying and pasting
	 */
	private String copyBuffer;
	
	/**
	 * Special toolbar that displays information
	 */
	private InformartionToolbar infoBar;
	
	/**
	 * Localization provider
	 */
	private LocalizationProvider provider = LocalizationProvider.getInstance();
	
	/**
	 * Used to prevent losing memory.
	 */
	private FormLocalizationProvider formP = new FormLocalizationProvider(provider, this);
	
	/**
	 * Constructor
	 */
	public JNotepadPP() {
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		setLocation(10, 10);
		setSize(900, 800);
		setTitle(provider.getString("NPTitle"));
		formP.addLocalizationListener(languageListener);
		
		initGUI();
	}
	

	/**
	 * Create all relevant objects for notepad
	 */
	private void initGUI() {
		Container cp = getContentPane();
		cp.setLayout(new BorderLayout());
		
		docModel = new DefaultMultipleDocumentModel();
		cp.add((Component) docModel, BorderLayout.CENTER);
		
		docModel.addChangeListener(nameListener);
		
		docModel.addMultipleDocumentListener(mdListener);
		
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				exit.actionPerformed(null);
			}
		});
		
		createActions();
		
		createMenus();
		
		createToolbar();
	}

	/**
	 * Set default values for all actions.
	 */
	private void createActions() {
		openBlankDocument.putValue(
				Action.NAME, 
				provider.getString("NPActOpBlkDocName"));
		openBlankDocument.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control N"));
		openBlankDocument.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_N);
		openBlankDocument.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActOpBlkDocShtDesc"));
		
		openExistingDocument.putValue(
				Action.NAME, 
				provider.getString("NPActOpExtDocName"));
		openExistingDocument.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control O"));
		openExistingDocument.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_O);
		openExistingDocument.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActOpExtDocShtDesc"));
		
		closeDocument.putValue(
				Action.NAME, 
				provider.getString("NPActCloseDocName"));
		closeDocument.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control C"));
		closeDocument.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_C);
		closeDocument.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActCloseDocShtDesc"));
		closeDocument.setEnabled(false);
		
		saveDocument.putValue(
				Action.NAME, 
				provider.getString("NPActSaveDocName"));
		saveDocument.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control S"));
		saveDocument.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_S);
		saveDocument.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActSaveDocShtDesc"));
		saveDocument.setEnabled(false);
		
		saveDocumentAs.putValue(
				Action.NAME, 
				provider.getString("NPActSaveAsDocName"));
		saveDocumentAs.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control A"));
		saveDocumentAs.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_A);
		saveDocumentAs.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActSaveAsDocShtDesc"));
		saveDocumentAs.setEnabled(false);
		
		copyText.putValue(
				Action.NAME, 
				provider.getString("NPActCpyTxtName"));
		copyText.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control C"));
		copyText.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_C);
		copyText.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActCpyTxtShtDesc"));
		
		pasteText.putValue(
				Action.NAME, 
				provider.getString("NPActPasteTextName"));
		pasteText.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control V"));
		pasteText.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_V);
		pasteText.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActPasteTextShtDesc"));
		
		cutText.putValue(
				Action.NAME, 
				provider.getString("NPActCutTextName"));
		cutText.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control X"));
		cutText.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_X);
		cutText.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActCutTextShtDesc"));
		
		exit.putValue(
				Action.NAME, 
				provider.getString("NPActExitName"));
		exit.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control E"));
		exit.putValue(
				Action.MNEMONIC_KEY, 
				KeyEvent.VK_E);
		exit.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActExitShtDesc"));
		
		statistics.putValue(
				Action.NAME, 
				provider.getString("NPActStatName"));
		statistics.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActStatShtDesc"));
		statistics.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control T"));
		
		toLowerCase.putValue(
				Action.NAME, 
				provider.getString("NPActToLwCName"));
		toLowerCase.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control shift L"));
		toLowerCase.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActToLwCShtDesc"));
		
		toUpperCase.putValue(
				Action.NAME, 
				provider.getString("NPActToUpCName"));
		toUpperCase.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control shift U"));
		toUpperCase.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActToUpShtDesc"));
		
		toggleCase.putValue(
				Action.NAME, 
				provider.getString("NPActTogCName"));
		toggleCase.putValue(
				Action.ACCELERATOR_KEY, 
				KeyStroke.getKeyStroke("control shift T"));
		toggleCase.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActTogCShtDesc"));
		
		changeLanguageEn.putValue(
				Action.NAME, 
				provider.getString("NPActChgLaEnName"));
		changeLanguageEn.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActChgLaEnShtDesc"));
		
		changeLanguageHr.putValue(
				Action.NAME, 
				provider.getString("NPActChgLaHrName"));
		changeLanguageHr.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActChgLaHrShtDesc"));
		
		changeLanguageDe.putValue(
				Action.NAME, 
				provider.getString("NPActChgLaDeName"));
		changeLanguageDe.putValue(
				Action.SHORT_DESCRIPTION, 
				provider.getString("NPActChgLaDeShtDesc"));
	}

	private JMenu file;
	private JMenu edit;
	private JMenu tools;
	private JMenu changeCase;
	private JMenu preferences;
	private JMenu changeLang;
	private JMenu sort;

	/**
	 * Create menus
	 */
	private void createMenus() {
		JMenuBar jb = new JMenuBar();
		
		file = new LJMenu("NPMenuFile", formP);
		jb.add(file);
		file.add(new JMenuItem(openBlankDocument));
		file.add(new JMenuItem(openExistingDocument));
		file.add(new JMenuItem(closeDocument));
		file.addSeparator();
		file.add(new JMenuItem(saveDocument));
		file.add(new JMenuItem(saveDocumentAs));
		file.addSeparator();
		file.add(new JMenuItem(exit));
		
		edit = new LJMenu("NPMenuEdit", formP);
		jb.add(edit);
		edit.add(new JMenuItem(copyText));
		edit.add(new JMenuItem(pasteText));
		edit.add(new JMenuItem(cutText));
		setJMenuBar(jb);
		
		tools = new LJMenu("NPMenuTools", formP);
		jb.add(tools);
		changeCase = new LJMenu("NPMenuChangeCase",formP);
		tools.add(changeCase);
		changeCase.add(toLowerCase);
		changeCase.add(toUpperCase);
		changeCase.add(toggleCase);
		
		preferences = new LJMenu("NPMenuPrefs", formP);
		jb.add(preferences);
		changeLang = new LJMenu("NPMenuLangC", formP);
		preferences.add(changeLang);
		changeLang.add(changeLanguageEn);
		changeLang.add(changeLanguageHr);
		changeLang.add(changeLanguageDe);
		
		sort = new LJMenu("NPMenuSort", formP);
		jb.add(sort);
		sort.add(sortAsc);
		sort.add(sortDesc);
	}

	/**
	 * Create toolbars
	 */
	private void createToolbar() {
		JToolBar tb = new JToolBar();
		
		tb.add(new JButton(openBlankDocument));
		tb.add(new JButton(openExistingDocument));
		tb.add(new JButton(closeDocument));
		tb.add(new JButton(saveDocument));
		tb.add(new JButton(saveDocumentAs));
		tb.add(new JButton(copyText));
		tb.add(new JButton(pasteText));
		tb.add(new JButton(cutText));
		tb.add(new JButton(statistics));
		
		tb.setFloatable(true);
		add(tb, BorderLayout.PAGE_START);
		
		infoBar = new InformartionToolbar(this);
		add(infoBar, BorderLayout.PAGE_END);
	}
	
	/**
	 * Action modeling the operation of opening a document
	 */
	private final Action openBlankDocument = new LocalizableAction("NPActOpBlkDocName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			openBlankDocumentFc();
		}
	};
	
	/**
	 * Open new blank document
	 */
	private void openBlankDocumentFc() {
		docModel.createNewDocument();
	}
	
	/**
	 * Action modeling the operation of opening an existing file
	 */
	private final Action openExistingDocument = new LocalizableAction("NPActOpExtDocName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			openExistingDocumentFc();
		}
	};
	
	/**
	 * Open existing document
	 */
	private void openExistingDocumentFc() {
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle("Open file");
		if (jfc.showOpenDialog(this) != JFileChooser.APPROVE_OPTION) {
			return;
		}
		
		Path filePath = jfc.getSelectedFile().toPath();
		
		SingleDocumentModel doc = docModel.loadDocument(filePath);
		if (doc == null) {
			return;
		}
	}
	
	/**
	 * Action modeling saving a document to file
	 */
	private final Action saveDocument = new LocalizableAction("NPActSaveDocName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			saveDocumentFc();
		}
	};
	
	/**
	 * Save a document to file
	 */
	private void saveDocumentFc() {
		docModel.saveDocument(docModel.getCurrentDocument(), null);
	}
	
	/**
	 * Action modeling saving a document under a specific name
	 */
	private final Action saveDocumentAs = new LocalizableAction("NPActSaveAsDocName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			saveDocumentAsFc();
		}
	};
	
	/**
	 * Save a document under a specific name
	 */
	private void saveDocumentAsFc() {
		JFileChooser jfc = new JFileChooser();
		jfc.setDialogTitle(provider.getString("MODELsaveDocumentFileChooser"));
		if (jfc.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) {
			JOptionPane.showMessageDialog(
					this, 
					provider.getString("MODELsaveDocumentFileChooserMsg"), 
					provider.getString("MODELsaveDocumentFileChooserName"), 
					JOptionPane.INFORMATION_MESSAGE);
			return;
		}
		Path newPath = jfc.getSelectedFile().toPath();
		docModel.saveDocument(docModel.getCurrentDocument(), newPath);
	}
	
	/**
	 * Action of closing a document form editor
	 */
	private final Action closeDocument = new LocalizableAction("NPActCloseDocName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			closeDocumentFc();
		}
	};
	
	/**
	 * Close a document in editor
	 */
	private void closeDocumentFc() {
		SingleDocumentModel curr = docModel.getCurrentDocument();
		if (curr.isModified()) {
			saveDocumentFc();
		}
		docModel.closeDocument(curr);
	}
	
	/**
	 * Action, copy a piece of text and save it
	 */
	private final Action copyText = new LocalizableAction("NPActCpyTxtName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			copy();
		}
	};
	
	/**
	 * Copy a piece of text from current document
	 */
	private void copy() {
		try {
			Document doc = docModel.getCurrentDocument().getTextComponent().getDocument();
			Caret car = docModel.getCurrentDocument().getTextComponent().getCaret();
			int min = Math.min(car.getDot(), car.getMark());
			int length = Math.abs(car.getDot() - car.getMark());
			copyBuffer = doc.getText(min, length);
		} catch (BadLocationException ex) {
		}
	}
	
	/**
	 * Action, insert text copied
	 */
	private final Action pasteText = new LocalizableAction("NPActPasteTextName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			paste();
		}
	};
	
	/**
	 * Insert piece of copied text into current document
	 */
	private void paste() {
		try {
			Document doc = docModel.getCurrentDocument().getTextComponent().getDocument();
			Caret car = docModel.getCurrentDocument().getTextComponent().getCaret();
			int min = Math.min(car.getDot(), car.getMark());
			cut();
			doc.insertString(min, copyBuffer, null);
		} catch (BadLocationException ex) {
		}
	}
	
	/**
	 * Action, remove a segment of text from document
	 */
	private final Action cutText = new LocalizableAction("NPActCutTextName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			cut();
		}
	};
	
	/**
	 * Remove text from document
	 */
	private void cut() {
		try {
			Document doc = docModel.getCurrentDocument().getTextComponent().getDocument();
			Caret car = docModel.getCurrentDocument().getTextComponent().getCaret();
			int min = Math.min(car.getDot(), car.getMark());
			int length = Math.abs(car.getDot() - car.getMark());
			doc.remove(min, length);
		} catch (BadLocationException ex) {
		}
	}
	
	/**
	 * Action display statistics
	 */
	private final Action statistics = new LocalizableAction("NPActStatName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			statistics();
		}
	};
	
	/**
	 * Show statics
	 */
	protected void statistics() {
		if (docModel.getCurrentDocument() == null) {
			return;
		}
		Document doc = docModel.getCurrentDocument().getTextComponent().getDocument();
		int length = doc.getLength();
		char[] text = null;
		char[] whites = {' ', '\t', '\n', '\r'};
		try {
			text = doc.getText(0, length - 1).toCharArray();
		} catch (BadLocationException e) {
		}
		int lines = 1;
		int spaces = 0;
		if (text != null) {
			for (char c : text) {
				if (c == '\n')
					lines++;
				for (char w : whites) {
					if (c == w)
						spaces++;
				}
			} 
		}
		JOptionPane.showMessageDialog(this, 
				String.format(provider.getString("NPInfoBar"), 
						length, lines, spaces));
	}
	
	/**
	 * Action of exiting from editor, files can be saved if the user agrees to save them.
	 */
	private final Action exit = new LocalizableAction("NPActExitName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			Iterator<SingleDocumentModel> iter = docModel.iterator();
			while(iter.hasNext()) {
				SingleDocumentModel doc = iter.next();
				if (doc.isModified()) {
					int option = JOptionPane.showConfirmDialog(
							JNotepadPP.this, 
							provider.getString("NPExitMsg") 
							+ (doc.getFilePath() != null ? doc.getFilePath().getFileName().toString() 
									: provider.getString("blankFileName")), 
							provider.getString("NPExitName") , 
							JOptionPane.YES_NO_CANCEL_OPTION, 
							JOptionPane.INFORMATION_MESSAGE);
					if (option == JOptionPane.YES_OPTION) {
						docModel.saveDocument(doc, null);
					} else if (option == JOptionPane.CANCEL_OPTION) {
						return;
					}
				}
			}
			infoBar.killClock();
			dispose();
		}
	};
	
	/**
	 * Change listener listening for tabs switching.
	 */
	private ChangeListener nameListener = new ChangeListener() {
		@Override
		public void stateChanged(ChangeEvent e) {
			String name = provider.getString("NPTitle") ;
			DefaultMultipleDocumentModel src = (DefaultMultipleDocumentModel) e.getSource();
			if (src.getNumberOfDocuments() == 0) {
				setTitle(name);
				return;
			}
			if (src.getCurrentDocument().getFilePath() == null) {
				setTitle(src.getTitleAt(src.getSelectedIndex()) + name);
				return;
			}
			setTitle(src.getCurrentDocument().getFilePath().toString() + name);
		}
	};
	
	/**
	 * Multiple document listener, updates some visuals and texts.
	 */
	private MultipleDocumentListener mdListener = new MultipleDocumentListener() {
		
		@Override
		public void documentRemoved(SingleDocumentModel model) {
		}
		
		@Override
		public void documentAdded(SingleDocumentModel model) {
		}
		
		@Override
		public void currentDocumentChanged(SingleDocumentModel previousModel, SingleDocumentModel currentModel) {
			previousModel.removeSingleDocumentListener(saveListener);
			previousModel.getTextComponent().getCaret().removeChangeListener(infoBar);
			previousModel.getTextComponent().removeCaretListener(toggleTextListener);
			if (currentModel != null) {
				currentModel.addSingleDocumentListener(saveListener);
				saveListener.documentModifyStatusUpdated(currentModel);
				
				currentModel.getTextComponent().getCaret().addChangeListener(infoBar);
				infoBar.stateChanged(new ChangeEvent(
						JNotepadPP.this.docModel.getCurrentDocument()
						.getTextComponent().getCaret()));
				
				currentModel.getTextComponent().addCaretListener(toggleTextListener);
				toggleTextListener.caretUpdate(new CaretEvent(this) {
					@Override
					public int getMark() {
						return 0;
					}
					@Override
					public int getDot() {
						return 0;
					}
				});
			}
		}
	};
	
	/**
	 * Listener, updates save actions from usable to unusable.
	 */
	private SingleDocumentListener saveListener = new SingleDocumentListener() {
		@Override
		public void documentModifyStatusUpdated(SingleDocumentModel model) {
			if (model == null) {
				saveDocumentAs.setEnabled(false);
			}
			saveDocument.setEnabled(docModel.getCurrentDocument().isModified());
			saveDocumentAs.setEnabled(true);
		}
		@Override
		public void documentFilePathUpdated(SingleDocumentModel model) {
		}
	};

	/**
	 * Get statistics on the current state in the document
	 * @param dot
	 * @return statistics
	 */
	protected StatInfo getStatInfo(int dot) {
		Document doc = docModel.getCurrentDocument().getTextComponent().getDocument();
		int length = doc.getLength();
		char[] text = null;
		try {
			text = doc.getText(0, dot).toCharArray();
		} catch (BadLocationException e) {
		}
		int line = 0;
		int lastLineIndex = 0;
		if (text != null) {
			for (int i = 0; i < text.length; i++) {
				if (text[i] == '\n') {
					line++;
					lastLineIndex = i;
				}
			}
		}
		int colm = text.length - lastLineIndex;
		return new StatInfo(line, colm, length);
	}
	
	/**
	 * Action transforming all letters to lower case
	 */
	private Action toLowerCase = new LocalizableAction("NPActToLwCName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			updateText( c -> Character.toLowerCase(c));
		}
	};
	
	/**
	 * Action transforming all letter to upper case
	 */
	private Action toUpperCase = new LocalizableAction("NPActToUpCName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			updateText( c -> Character.toUpperCase(c));
		}
	};
	
	/**
	 * Action toggles all letters in case
	 */
	private Action toggleCase = new LocalizableAction("NPActTogCName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			updateText( c -> {
				if (Character.isUpperCase(c)) {
					return Character.toLowerCase(c);
				} else {
					return Character.toUpperCase(c);
				}
			});
		}
	};
	
	/**
	 * Updates text using a function
	 * @param cFunc
	 */
	private void updateText(Function<Character, Character> cFunc) {
		Caret car= docModel.getCurrentDocument().getTextComponent().getCaret();
		int min = Math.min(car.getDot(),car.getMark());
		 int len = Math.abs(car.getDot() - car.getMark());
		 
		 if (len < 1) {
			 return;
		 }
		 
		 Document doc = docModel.getCurrentDocument().getTextComponent().getDocument();
		 try {
			String text = doc.getText(min, len);
			char[] chars = text.toCharArray();
			for (int i = 0; i < chars.length; i++) {
				chars[i] = cFunc.apply(chars[i]);
			}
			text = new String(chars);
			doc.remove(min, len);
			doc.insertString(min, text, null);
		} catch (BadLocationException ignorables) {
		}
	}
	
	/**
	 * Listener for text updating, must be some text selected
	 */
	private CaretListener toggleTextListener = new CaretListener() {
		@Override
		public void caretUpdate(CaretEvent e) {
			boolean set = e.getDot() != e.getMark();
			toUpperCase.setEnabled(set);
			toLowerCase.setEnabled(set);
			toggleCase.setEnabled(set);
		}
	};
	
	/**
	 * Change language to english
	 */
	private Action changeLanguageEn = new LocalizableAction("NPActChgLaEnName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			provider.setLanguage("en");
		}
	};
	
	/**
	 * Change language to croatian
	 */
	private Action changeLanguageHr = new LocalizableAction("NPActChgLaHrName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			provider.setLanguage("hr");
		}
	};
	
	/**
	 * Change language to german
	 */
	private Action changeLanguageDe = new LocalizableAction("NPActChgLaDeName", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			provider.setLanguage("de");
		}
	};
	
	private ILocalizationListener languageListener = new ILocalizationListener() {
		@Override
		public void localizationChanged() {
			createActions();
		}
	};
	
	/**
	 * Sort selected lines ascending
	 */
	private Action sortAsc = new LocalizableAction("NPActSortAsc", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			sort((s1, s2) -> {
				Collator coll = Collator.getInstance(new Locale(provider.getCurrentLanguage()));
				return coll.compare(s1, s2);
			});
		}
	};
	
	/**
	 * Sort selected lines descending
	 */
	private Action sortDesc = new LocalizableAction("NPActSortDesc", formP) {
		@Override
		public void actionPerformed(ActionEvent e) {
			sort((s1, s2) -> {
				Collator coll = Collator.getInstance(new Locale(provider.getCurrentLanguage()));
				return coll.compare(s2, s1);
			});
		}
	};
	
	@SuppressWarnings("unused")
	private void sort(Comparator<String> comp) {
		Document doc = docModel.getCurrentDocument().getTextComponent().getDocument();
		Caret car = docModel.getCurrentDocument().getTextComponent().getCaret();
		int start = Math.min(car.getDot(), car.getMark());
		int end = Math.max(car.getDot(), car.getMark());
		char[] line = null;
		try {
			char[] text = doc.getText(0, doc.getLength()).toCharArray();
			while(start > 0 && text[start] != '\n') start--;
			while(end < doc.getLength() - 1 && text[end] != '\n') end++;
			line = doc.getText(start, end).toCharArray();
		} catch (BadLocationException e1) {
			return;
		}
		int lSt = 0;
		List<String> lines = new LinkedList<>();
		for (int i = 0; i < line.length; i++) {
			if (line[i] == '\n') {
				//TODO završi ovo
			}
		}
	}
	
	/**
	 * Main method, launches the editor with no files open.
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater( () -> {
			new JNotepadPP().setVisible(true);
		});
	}
}
