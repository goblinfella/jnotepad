package hr.fer.zemris.java.hw11.jnotepadpp;

import java.awt.Image;
import java.io.IOException;
import java.io.InputStream;

import javax.swing.Icon;
import javax.swing.ImageIcon;

/**
 * Utility class for {@link JNotepadPP}
 * @author Fabijan Džapo
 *
 */
public class Util {
	
	/**
	 * Retrieves desired icon
	 * @param iconPath
	 * @return icon
	 */
	private Icon getIcon(String iconPath) {
		ImageIcon icon = null;
		try {
			InputStream is = this.getClass().getResourceAsStream(iconPath);
			byte[] bytes = is.readAllBytes();
			is.close();
			icon = new ImageIcon(bytes);
			Image im = icon.getImage();
			im = im.getScaledInstance(20, 20, Image.SCALE_SMOOTH);
			icon = new ImageIcon(im);
		} catch (IOException | NullPointerException e) {
			System.out.println("joj");
		}
		return icon;
	}
	
	/**
	 * Get green floppy disc icon
	 * @return green icon
	 */
	public Icon getGreen() {
		return getIcon("icons/green.png");
	}
	
	/**
	 * Get red floppy disc icon
	 * @return red icon
	 */
	public Icon getRed() {
		return getIcon("icons/red.png");
	}

}
