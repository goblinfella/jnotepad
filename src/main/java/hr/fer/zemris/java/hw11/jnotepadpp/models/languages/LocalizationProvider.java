package hr.fer.zemris.java.hw11.jnotepadpp.models.languages;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizationProvider extends AbstractLocalizationProvider {
	
	private static LocalizationProvider singleton;
	
	private String language;
	
	private ResourceBundle bundle;
	
	private LocalizationProvider(){
		language = "en";
		bundle = ResourceBundle.getBundle("hr.fer.zemris.java.hw11.jnotepadpp.languages.language", 
				Locale.forLanguageTag(language));
	}
	
	public static LocalizationProvider getInstance() {
		if (singleton == null) {
			singleton = new LocalizationProvider();
		}
		return singleton;
	}

	public void setLanguage(String language) {
		if (this.language.equals(language)) return;
		this.language = language;
		bundle = ResourceBundle.getBundle("hr.fer.zemris.java.hw11.jnotepadpp.languages.language", 
				Locale.forLanguageTag(language));
		fire();
	}

	@Override
	public String getString(String key) {
		return bundle.getString(key);
	}

	@Override
	public String getCurrentLanguage() {
		return language;
	}

}
