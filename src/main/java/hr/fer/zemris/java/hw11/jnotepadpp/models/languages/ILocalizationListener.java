package hr.fer.zemris.java.hw11.jnotepadpp.models.languages;

/**
 * Interface modeling a listener of a LocalizationProvider
 * @author Fabijan Džapo
 *
 */
public interface ILocalizationListener {
	
	/**
	 * The localization has changed
	 */
	void localizationChanged();

}
