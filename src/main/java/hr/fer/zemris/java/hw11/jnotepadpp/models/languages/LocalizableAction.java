package hr.fer.zemris.java.hw11.jnotepadpp.models.languages;

import javax.swing.AbstractAction;

/**
 * Action that can update it's info based on localized settings.
 * @author Fabijan Džapo
 *
 */
@SuppressWarnings("serial")
public abstract class LocalizableAction extends AbstractAction {
	
	/**
	 * Constructor, takes key to name and localization provider.
	 * @param key
	 * @param lp
	 */
	public LocalizableAction(String key, ILocalizationProvider lp) {
		putValue(key, lp.getString(key));
		lp.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				putValue(key, lp.getString(key));
			}
		});
	}
}
