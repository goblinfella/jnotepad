package hr.fer.zemris.java.hw11.jnotepadpp.models.languages;

import javax.swing.JLabel;

/**
 * Label that can update it's text based on localised settings.
 * @author Fabijan Džapo
 *
 */
@SuppressWarnings("serial")
public class LJLabel extends JLabel {
	
	public LJLabel(String key, ILocalizationProvider lp) {
		setText(lp.getString(key));
		lp.addLocalizationListener(new ILocalizationListener() {
			@Override
			public void localizationChanged() {
				setText(lp.getString(key));
			}
		});
	}

}
