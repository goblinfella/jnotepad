package hr.fer.zemris.java.hw11.jnotepadpp;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.MultipleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentListener;
import hr.fer.zemris.java.hw11.jnotepadpp.models.SingleDocumentModel;
import hr.fer.zemris.java.hw11.jnotepadpp.models.languages.ILocalizationProvider;
import hr.fer.zemris.java.hw11.jnotepadpp.models.languages.LocalizationProvider;

/**
 * A class modeling a default implementation of multiple documents.
 * @author Fabijan Džapo
 *
 */
@SuppressWarnings("serial")
public class DefaultMultipleDocumentModel extends JTabbedPane implements MultipleDocumentModel{
	
	/**
	 * Documents
	 */
	private List<SingleDocumentModel> documentModels;
	
	/**
	 * Listeners
	 */
	private List<MultipleDocumentListener> listeners;
	
	/**
	 * Last model edited before the current
	 */
	private SingleDocumentModel prior;
	
	/**
	 * Utility class
	 */
	private Util util = new Util();
	
	private ILocalizationProvider provider;
	
	/**
	 * Constructor
	 */
	public DefaultMultipleDocumentModel() {
		super();
		documentModels = new ArrayList<>();
		listeners = new LinkedList<>();
		addChangeListener(tabchange);
		provider = LocalizationProvider.getInstance();
	}

	@Override
	public Iterator<SingleDocumentModel> iterator() {
		return documentModels.iterator();
	}

	@Override
	public SingleDocumentModel createNewDocument() {
		SingleDocumentModel newDoc = new DefaultSingleDocumentModel(null, "");
		documentModels.add(newDoc);
		addTab(provider.getString("blankFileName") ,
				util.getGreen(),
				new JScrollPane(newDoc.getTextComponent()));
		setFocus(documentModels.indexOf(newDoc));
		setListener(newDoc);
		notifyListenersAdded();
		return newDoc;
	}

	private void setFocus(int index) {
		setSelectedIndex(index);
	}

	@Override
	public SingleDocumentModel getCurrentDocument() {
		return getDocument(getSelectedIndex());
	}

	@Override
	public SingleDocumentModel loadDocument(Path path) {
		if(!Files.isReadable(path)) {
			JOptionPane.showMessageDialog(
					this, 
					String.format(provider.getString("MODELloadDocumentReadingErrorMsg") , 
							path.toString()), 
					provider.getString("MODELloadDocumentReadingErrorName"), 
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
		String text = null;
		try {
			text = Files.readString(path);
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(
					this, 
					provider.getString("MODELloadDocumentErrorMsg"), 
					provider.getString("MODELloadDocumentErrorName"), 
					JOptionPane.ERROR_MESSAGE);
			return null;
		}
		
		for (SingleDocumentModel model : documentModels) {
			if (model.getFilePath() == null) continue;
			if (model.getFilePath().equals(path)) {
				setFocus(documentModels.indexOf(model));
				return null;
			}
		}
		SingleDocumentModel doc = new DefaultSingleDocumentModel(path, text);
		documentModels.add(doc);
		addTab(path.getFileName().toString(), 
				util.getGreen(), 
				new JScrollPane(doc.getTextComponent()), 
				path.toString());
		setFocus(documentModels.indexOf(doc));
		setListener(doc);
		notifyListenersAdded();
		return doc;
	}
	
	/**
	 * Create a listener for a model.
	 * @param model
	 */
	private void setListener(SingleDocumentModel mod) {
		mod.addSingleDocumentListener( new SingleDocumentListener() {
			
			@Override
			public void documentModifyStatusUpdated(SingleDocumentModel model) {
				if (model.isModified()) {
					setIconAt(getSelectedIndex(), util.getRed());
				} else {
					setIconAt(getSelectedIndex(), util.getGreen());
				}
			}
			
			@Override
			public void documentFilePathUpdated(SingleDocumentModel mod) {
				int index = documentModels.indexOf(mod);
				setTitleAt(index, mod.getFilePath().getFileName().toString());
				setToolTipTextAt(index, mod.getFilePath().toString());
			}
		});
	}

	@Override
	public void saveDocument(SingleDocumentModel model, Path newPath) {
		if (newPath != null) {
			model.setFilePath(newPath);
		}
		
		if (model.getFilePath() == null) {
			JFileChooser jfc = new JFileChooser();
			jfc.setDialogTitle(provider.getString("MODELsaveDocumentFileChooser"));
			if (jfc.showSaveDialog(this) != JFileChooser.APPROVE_OPTION) {
				JOptionPane.showMessageDialog(
						this, 
						provider.getString("MODELsaveDocumentFileChooserMsg"), 
						provider.getString("MODELsaveDocumentFileChooserName"), 
						JOptionPane.INFORMATION_MESSAGE);
				return;
			}
			newPath = jfc.getSelectedFile().toPath();
		}
		
		if (newPath != null && Files.exists(newPath)) {
			int choice = JOptionPane.showConfirmDialog(this, 
					provider.getString("MODELsaveDocumentFileExistMsg"));
			if (choice == JOptionPane.YES_OPTION) {
				model.setFilePath(newPath);
			} else if (choice == JOptionPane.NO_OPTION) {
				return;
			}
		}
		
		for (SingleDocumentModel sdmodel : documentModels) {
			if (sdmodel.getFilePath() == null) continue;
			if (sdmodel.equals(model)) continue;
			if (sdmodel.getFilePath().equals(newPath)) {
				JOptionPane.showMessageDialog(this, 
						provider.getString("MODELsaveDocumentFileOpenErrorMsg"), 
						provider.getString("MODELsaveDocumentFileOpenErrorName"),
						JOptionPane.ERROR_MESSAGE);
				return;
			}
		}
		
		try {
			Files.writeString(
					model.getFilePath(), 
					model.getTextComponent().getText());
		} catch (IOException e1) {
			JOptionPane.showMessageDialog(
					this, 
					provider.getString("MODELsaveDocumentFileSavingErrorMsg"), 
					provider.getString("MODELsaveDocumentFileSavingErrorName"), 
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		JOptionPane.showMessageDialog(
				this, 
				provider.getString("MODELsaveDocumentSavedMsg"), 
				provider.getString("MODELsaveDocumentSavedName"), 
				JOptionPane.INFORMATION_MESSAGE);
		model.setModified(false);
	}

	@Override
	public void closeDocument(SingleDocumentModel model) {
		int index = documentModels.indexOf(model);
		documentModels.remove(model);
		removeTabAt(index);
		notifyListenersRemoved();
	}

	@Override
	public void addMultipleDocumentListener(MultipleDocumentListener l) {
		if (l == null) {
			throw new IllegalArgumentException("Listener cannot be null.");
		}
		listeners = new LinkedList<>(listeners);
		listeners.add(l);
	}

	@Override
	public void removeMultipleDocumentListener(MultipleDocumentListener l) {
		if (l == null) {
			throw new IllegalArgumentException("Listener cannot be null.");
		}
		listeners = new LinkedList<>(listeners);
		listeners.remove(l);
	}
	
	private void notifyListenersAdded() {
		for (MultipleDocumentListener mdl : listeners) {
			mdl.documentAdded(getCurrentDocument());
		}
	}
	
	private void notifyListenersRemoved() {
		for (MultipleDocumentListener mdl : listeners) {
			mdl.documentRemoved(getCurrentDocument());
		}
	}
	
	private void notifyListenersChanged(SingleDocumentModel prev) {
		for (MultipleDocumentListener mdl : listeners) {
			mdl.currentDocumentChanged(prev, getCurrentDocument());
		}
	}
	
	private ChangeListener tabchange = new ChangeListener() {
		@Override
		public void stateChanged(ChangeEvent e) {
			if (prior == null) prior = getCurrentDocument();
			notifyListenersChanged(prior);
			prior = getCurrentDocument();
		}
	};

	@Override
	public int getNumberOfDocuments() {
		return documentModels.size();
	}

	@Override
	public SingleDocumentModel getDocument(int index) {
		if (index < 0) return null;
		return documentModels.get(index);
	}

}
